"""
Funções para realizar as operaçoes
"""

#função para receber dois valores e retornar a soma deles
def soma(v1,v2):
    return v1+v2

#função para receber dois valores e retornar a subtração deles
def sub(v1,v2):
    return v1-v2

#função para receber dois valores e retornar a multiplicação deles
def mult(v1,v2):
    return v1*v2

#função para receber dois valores e retornar a divisão deles, além de impedir a divisão por zero
def divi(v1,v2):
    if v2!=0:
        return v1/v2
    
    else:
        return 'Erro: Impossível dividir algum número por zero'

def porcentagem(v1,v2):
    return v1*(v2/100)

#funçao sem retorno para parar o programa caso seja digitado s
def sair(v1,v2,v3):
    if v1=='s' or v1=='S' or v2=='s' or v2=='S' or v3=='s' or v3=='S':
        quit()
    
"""def numero(v1,v2,erro):
    erro=0
    for i in range (len(v1)):
            if v1[i].isalpha() and v1[i]!='.':
                print('\nVocê inseriu algo que não é um número no primeiro valor, tente novamente!\n')
                erro=1
                break
                
    for i in range (len(v2)):
        if v2[i].isalpha() and v2[i]!='.':
            print('\nVocê inseriu algo que não é um número no segundo valor, tente novamente!\n')
            erro=1 
            break"""
    
